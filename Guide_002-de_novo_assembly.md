---
title: De Novo Genome Assembly with MinION Long Fuzzy Reads
author: Jeff Paril
date: 2018 August 17
documentclass: scrartcl
papersize: a4
geometry: "left=1cm, right=1cm, top=2cm, bottom=2cm"
fontsize: 9pt
tags: [minion, MinKNOW, nanopore, nanoporetech, Oxford Nanopore, de novo genome assembly, Lolium rigidum, rigid ryegrass, annual ryegrass, minimap2, miniasm, canu, WIMP]
titlepage: true
# titlepage-color: 4daf4a
titlepage-color: 8BC34A
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "FFFFFF"
titlepage-rule-height: 2
---

\pagebreak

# Guide 002

## De Novo Genome Assembly with MinION Long Fuzzy Reads of a Heterozygous Genome (*Lolium rigidum*, Gaudin 1811)

# DNA EXTRACTION

## Materials and Equipment

- mortar and pestle
- tiny spatula
- 50 ml falcon tubes
- 1.5 ml tubes
- pipettes and tips (p10, p100 & p1000)
- water bath at 60&deg;C-65&deg;C (heat block + 1L glass beaker)
- microcentrifuge
- fume hood


## Reagents
- CTAB DNA extraction buffer

| Reagent                 | Final concentration | Amount to add for 250 ml |
|:------------------------|--------------------:|-------------------------:|
| Tris (1 M, pH 7)        |              100 mM |                    25 ml |
| NaCl (5 M)              |              700 mM |                    35 ml |
| EDTA (0.25 M, pH 8)     |               10 mM |                    10 ml |
| CTAB                    |                  1% |                    2.5 g |
| $H_{2}O$                |                   - |    volume up to 249.5 ml |
| $\beta$-mercaptoethanol |                0.2% |                   0.5 ml |

where:

| Reagent    | Molecular mass | Directions (autoclave & store at room temperature) |
|:-----------|:--------------:|:---------------------------------------------------|
| 1M Tris    |  121.14g/mol   | 60.57g in 400ml; pH7 with HCl; volume up 500ml     |
| 5M NaCl    |   58.4g/mol    | 146g in 400ml; volume to 500ml                     |
| 0.25M EDTA |  292.24g/mol   | 29.22g in 320ml with 4g NaOH; pH8; volume up 400ml |

- Chloroform:isoamyl alcohol (24:1)
- Ethanol (70%)
- Isopropanol
- TE buffer (pH 8)

| Reagent             | Final concentration | Amount to add for 250 ml |
|:--------------------|--------------------:|-------------------------:|
| Tris (1 M, pH 7)    |               10 mM |                   2.5 ml |
| EDTA (0.25 M, pH 8) |                1 mM |                     1 ml |

## Procedure

1. Prepare leaf tissues (pool ~100 samples of ~100mg or ~1cm^2^ leaf tissues each, for a total of ~10g into a 50-ml falcon tube)
2. **Fume hood**: Prepare 10 ml of CTAB in a 50ml-falcon tube (~10 ml CTAB + 20 $\mu$l $\beta$-mercaptoethanol), do not close cap tightly and heat up in water bath at 60&deg;C - 65&deg;C
3. Grind leaf tissues into fine powder with mortar and pestle in liquid nitrogen (top up liquid nitrogen multiple times as necessary)
4. Place ~100 $\mu$l of powdered leaf tissues (~5 spatula-fulls) into each 1.5ml tube (do not close)
5. **Fume hood**: Add 1.0ml of pre-heated CTAB solution into the 1.5ml tube containing the powdered leaf tissues, close the lid once the leaf tissues have thawed in the solution, and mix by inversion
6. **Fume hood**: Incubate the CTAB-leaf sludge at 60&deg;C - 65&deg;C for 30 minutes while mixing every 10 minutes [lyse the cells] + Note: During the first mixing, open and close the caps to equilibrate pressures
7. Centrifuge at 10,000g $\big( RPM = \sqrt{g \over {(1.118 \times 10^{-5}) \times rotor\_radius (cm)}}\big)$ for 2 minutes
8. **Fume hood**: Transfer 750$\mu$l of aqueous layer into a new 1.5ml tube taking care not to include any leaf tissues
9. **Fume hood**: Add 750$\mu$l (equal volume) of 24:1 chloroform:isoamyl alcohol, mix by gentle inversion, and centrifuge at 10,000g for 5 minutes [denature and separate the proteins]
10. **Fume hood**: Transfer 500$\mu$l of the uppermost aqueous phase into a fresh 1.5ml tube, taking care not to include any of the interphase [isolate nucleic acids (both DNA and RNA) from the denatured proteins]
11. Add 100$\mu$l (1/5 volume) of 5M NaCl and 500$\mu$l (equal volume) of cold isopropanol, mix by gentle inversion, and incubate at room temperature for 30 minutes [precipitate nucleic acids]
12. Centrifuge at 10,000g  for 15 minutes at room temperature [pellet the nucleic acids]
13. Discard the supernatant, add 1 ml of cold 70% ethanol, clean the pellet and the tube by inversion, and discard the supernatant
14. Add 500$\mu$l of cold 70% ethanol, and centrifuge at 10,000g for 2 minutes
15. **Fume hood**: Discard supernatant, and dry the pellet under the fume hood for about 15 minutes
16. Dissolve DNA in 22$\mu$l nuclease-free water (1$\mu$l for Nanodrop quality check and 1$\mu$l for Qubit DNA quantification)

# LIBRARY PREPARATION

## End repair or A-tailing

- Materials:
    * size-selected DNA samples
    * nuclease-free water (or sterile reverse osmosis (RO) water)
    * 500$\mu$l eppy tube
    * P100 pipette and tips
    * P10 pipette and tips
    * Ultra II End-Prep buffer
    * Ultra II End-Prep enzyme mix
    * Heat block
    * NucleMag beads
    * magnetic separator eppy tube rack
    * 70% ethanol

- Dilute DNA samples to equal concentrations and volume up each sample to $\ge$ 45$\mu$l while targeting **$\ge$ 1000 ng of DNA material**
- Prepare End-repair/dA-tailing master mix, add 15$\mu$l to each 45$\mu$l DNA solution, and mix by pipetting
- Incubate at 20&deg;C (or RT) for 5 minutes and at 65&deg;C for 5 minutes
- Add equal volume (60$\mu$l) of homogenized NucleoMag beads at RT, and mix by pipetting
- Incubate at RT for 5 minutes, and pellet on magnet
- While on the magnetic rack, wash with 200$\mu$l 70% ethanol and remove the supernatant without disturbing the beads
- Repeat ethanol wash
- Spin down, place on the magnet and pipette out residual wash with p10
- Dry for ~5 minutes
- Elute the DNA in 25$\mu$l nuclease-free water, mix by pipetting, and incubate for 2-5 minutes
- Magnet separate for 5 minutes and transfer dA-tailed DNA into a new 500$\mu$l eppy tube

| Reagents                     | 1X 60$\mu$l Reaction ($\mu$l) |
|:-----------------------------|------------------------------:|
| Nuclease-Free Water          |                             5 |
| Ultra II End-Prep Buffer     |                             7 |
| Ultra II End-Prep Enzyme Mix |                             3 |
| digested gDNA                |                            45 |

Table: End-repair or dA-tailing mix

## Quantification of digested, size-selected, dA-tailed, barcoded DNA samples (Qubit dsDNA HS quantification)

- Materials:
    * size-selected DNA samples (1$\mu$l each)
    * nuclease-free water (or sterile reverse osmosis (RO) water)
    * 200$\mu$l Qubit tubes
    * P1000 pipette and tips
    * P100 pipette and tips
    * P10 pipette and tips
    * Qubit dsDNA HS Buffer
    * Qubit dsDNA HS Reagent (orange solution)
    * Qubit dsDNA HS Standard 1 (10$\mu$l)
    * Qubit dsDNA HS Standard 2 (10$\mu$l)

- Prepare Qubit working solution: 1$\mu$l Qubit reagent + 199$\mu$l Qubit Buffer for each DNA sample (2 standards + number of DNA samples to quantify)
- Place 1$\mu$l of DNA sample into each Qubit tubes and 10$\mu$l each of the 2 standards in separate tubes
- Add the Qubit working solution, 199$\mu$l into the sample tubes and 190$\mu$l into the two standards
- Load the standards first to calibrate
- Read the samples one at a time (you may the samples multiple times for replication)

## Ligation of MinION-specific adaptors (tethering with)

- Materials:
    * barcoded DNA samples
    * nuclease-free water (or sterile reverse osmosis (RO) water)
    * 500$\mu$l eppy tube
    * P100 pipette and tips
    * P10 pipette and tips
    * Barcode Adapter Mix (BAM from EXP-NBD103 barcoding kit)
    * NEBNext Quick Ligation Reaction Buffer
    * Quick T4 DNA Ligase
    * NucleMag beads
    * magnetic separator eppy tube rack
    * Adapter Bead Binding Buffer (ABB from SQK-LSK108 ligation kit)
    * Elution Buffer (ELB from SQK-LSK108 ligation kit)
    * Heat block

- Dilute each DNA sample into equal concentrations and pool to a total volume of 50$\mu$l containing a total of 700ng of DNA material (concentrate DNA via NucleoMag beads purification 1:1 ratio, if too dilute)
- Prepare NEBNext Quick Ligation master mix, add equal volume (50$\mu$l) to the DNA samples, and mix by pipetting
- Incubate at RT for 10 minutes
- Add equal volume (100$\mu$l) of homogenized NucleoMag beads and mix by pipetting
- Incubate at RT for 5 minutes, magnet separate, and discard supernatant
- Add 140$\mu$l Adapter Bead Binding Buffer (ABB), mix by pipetting, magnet separate, and discard supernatant. Repeat this step.
- Elute the DNA in 15$\mu$l elution buffer (ELB), mix by pipetting, and incubate at 37&deg;C for 10 minutes
- Magnet separate for 5 minutes and transfer prepared DNA library into a new 500$\mu$l eppy tube (Optional step to re-quantify with the aim of yielding ~200ng of DNA material)

| Reagents                               | 100$\mu$l Reaction ($\mu$l) |
|:---------------------------------------|----------------------------:|
| Barcoded DNA                           |                          50 |
| Barcode Adapter Mix (BAM)              |                          20 |
| NEBNExt Quick Ligation Reaction Buffer |                          20 |
| Quick T4 DNA Ligase                    |                          10 |

Table: NEBNext Quick Ligation mix

## Prepare library for loading

- Materials:
    * ligated barcoded dA-tailed size-selected digested genomic DNA samples
    * 500$\mu$l eppy tube
    * P100 pipette and tips
    * P10 pipette and tips
    * nuclease-free water (or sterile reverse osmosis (RO) water)
    * Running buffer with fuel mix (RBF from SQK-LSK108 ligation kit)
    * Library loading beads (LLB from SQK-LSK108 ligation kit)

- Prepare the priming mix by mixing 480$\mu$l RBF with 520$\mu$l nuclease-free water
- Mix 35$\mu$l RBF and 25.5$\mu$l LLB with 12$\mu$l of the adapted and tethered DNA library and volume up to 75$\mu$l with nuclease-free water

| Reagents                           | 100$\mu$l Reaction ($\mu$l) |
|:-----------------------------------|----------------------------:|
| Running Buffer with Fuel Mix (RBF) |                         480 |
| Nuclese-free Water                 |                         520 |

Table: Priming mix

| Reagents                           | 100$\mu$l Reaction ($\mu$l) |
|:-----------------------------------|----------------------------:|
| DNA library (adapted and tethered) |                        12.0 |
| Running Buffer with Fuel Mix (RBF) |                        35.0 |
| Library Loading Beads (LLB)        |                        25.5 |
| Nuclease-free Water                |                         2.5 |

Table: Library loading mix


## Flow cell priming and sample loading

- Open the priming port by sliding the cover clockwise
- Remove inlet channel air plug
    + set P1000 pipette to 200$\mu$l
    + insert the tip into the priming port
    + keeping the tip inserted in the port, adjust the volume by twisting counter-clockwise until you reach 220-230$\mu$l, or until you see a small amount of the yellow buffer in the tip
    + remove pipette from the port and discard the tip
- Flow cell priming
    + prepare the priming mix (480$\mu$l RBF + 520$\mu$l nuclease-free water)
    + load 800$\mu$l of the priming mix into the priming port slowly
    + do not load all the mix keep a small amount at the tip
    + close the priming port
    + wait for 5 minutes
    + re-open the priming port by sliding the cover clockwise
    + open the sample port by gently lifting the sample port cover
    + load the remaining 200$\mu$l of the priming mix into the priming port using P1000 slowly to prevent overflowing in the sample port (do not empty the tip)
- Resuspend the library loading mix by gently pipetting up and down with P100
- Load 75$\mu$l of the the library loading mix into the sample port using P100 **one drop at a time** (wait for the previous drop to drain into the flow cell before adding the next drop)
- Close the sample port with the cover (ensure the bung enters the port)
- Close the priming port (turn counter-clockwise)
- Close the MinION lid carefully (magnetic at the edge)

# SEQUENCING

## Initialize the MinKNOW software

~~~~~~~~~~
sudo systemctl daemon-reload
sudo systemctl enable minknow
sudo systemctl start minknow
cd /opt/ui/
./MinKNOW
~~~~~~~~~~

- Tick the "Available" box and choose the proper flow cell type (i.e. FLO-MIN106 or FLO-MIN107)
- Click "New Experiment" and in the pop-up screen:
    + enter experiment name
    + select the proper kit (e.g. SQK-LSK108)
    + turn basecalling on or off depending on your machine (at least 16GB RAM and 1TB SSD is needed if you want this turned on)
    + set run options or keep default at 48 hours and -180mV
    + specify output files (just .fast5 files for separate basecalling; the number of files written per folder can also be specified)
    + Click "Start run"
- When sequencing is initiated:
    + the software will move to the channel screen
    + flow cell will cycle through all 2048 channels to select the best set of 512 to start the run
    + once the best combination of channels is selected, sequencing will begin on those channels automatically
    + within the first half an hour you should have most active pores sequencing and already have obtained several thousand reads
- After 6 hours (or however long you set):
    + you can stop the run from the home screen
    + output will be in /var/lib/MinKNOW/data/reads

## Washing and storing the flow cell for future use

- Open the priming port by turning the cover 90&deg; clockwise
- Remove bubbles in the small section between the priming port and the sensor array by inserting p1000 tip into the priming port and twisting the pipette to increase the volume until the bubbles are captured/removed
- Add 150$\mu$l of Washing Kit Solution A through the priming port slowly, and wait 10 minutes
- For later re-use or for storage:
    + Slowly add 500$\mu$l of Washing Kit Storage Buffer (Solution S) through the priming port
    + Close the priming port and remove half or all the buffer from the waste section through either waste ports
    + Store flow cell at 4-8&deg;C
- For immediate re-use:
    +  Slowly add 150$\mu$l of Washing Kit Solution B
    +  Re-prime the flow cell

## Basecalling

Locate output files in /var/lib/MinKNOW/data/reads/{experiment_name}/. Copy *.fast5* files into a high performance computing machine and move all *.fast5* files into a single directory:

~~~~~~~~~~~
scp /var/lib/MinKNOW/data/reads/20180_*/ user@123.456.789://some/directory/RAW_DATA

ssh user@123.456.789
cd /some/directory/RAW_DATA
for d1 in 201808*/
do
for d2 in ${d1}fast5/*/
do
echo $d2
mv ${d2}*.fast5 .
done
done
~~~~~~~~~~~

Convert fast5 raw reads into fastq sequence format via basecalling with *albacore*:

~~~~~~~~~~~
DIR=~/DRIVE/data/Lolium/Genomics/SEQUENCES/DNA/MinION/002*

read_fast5_basecaller.py \
    --flowcell FLO-MIN106 \
    --kit SQK-LSK108 \
    --barcoding \
    --output_format fastq \
    --input ${DIR}/RAW_DATA \
    --save_path ${DIR}/FASTQ_OUT \
    --worker_threads 10
~~~~~~~~~~~

Assess read lengths and number of reads with this tiny C code *fastqSTATS.c*:

~~~~~~~~~~~
#include <stdio.h>
#include <string.h>
#define bufSize 32768
//increase buffer size accordingly for longer reads

int main(int argc, char *argv[])
{
    //setup variables
    FILE* fp;
    char buf[bufSize];
    int LINENUM;
    int READLEN;
    int A,T,C,G;
    int SUMQ;

    //check file input
    if (argc != 2){
        fprintf(stderr, "Usage: %s <input-file>\n", argv[0]);
        return 1;
    }

    //read file
    if ((fp = fopen(argv[1], "r")) == NULL){
        /* Open source file. */
        perror("fopen source-file");
        return 1;
    }

    //iterate per line and parse only the second line of each line quartet as is the format of fastq files
    LINENUM=-1;
    while (fgets(buf, sizeof(buf), fp) != NULL) {
        buf[strlen(buf) - 1] = '\0';
        //count the number of A,T,C and G
        if (LINENUM % 4 == 0){
            READLEN=strlen(buf);
            A=0;T=0;C=0;G=0;
            for (int i=0; i<strlen(buf); i++){
                switch (buf[i]){
                    case 65:
                        A++;
                    case 84:
                        T++;
                    case 67:
                        C++;
                    case 71:
                        G++;
                }
            }
        }
         //summation of quality scores per base
        if ((LINENUM-2) % 4 == 0){
            SUMQ=0;
            for (int i=0; i<strlen(buf); i++){
                //ASCII code from 33-75 convert to Q 0-42
                SUMQ = SUMQ + (buf[i]-33);
            }
            //output filename, readlength
            printf("%i,%s,%i,%i,%i,%i,%i,%i\n", LINENUM-4,argv[1],READLEN,A,T,C,G,SUMQ);
        }
        LINENUM++;
    }
    fclose(fp);
    return 0;
}
~~~~~~~~~~~

Compile and execute *fastQC.c*:

~~~~~~~~~~~
DIR=~/DRIVE/data/Lolium/Genomics/SEQUENCES/DNA/MinION/002*

cd ${DIR}/FASTQ_OUT/
gcc fastqSTATS.c -o fastqSTATS

for i in *.fastq
do
echo $i
    ./fastqSTATS $i > ${i%.*}_READSTATS.csv
done

echo "LINE,PASS,BARCODE,READLENGTH,A,T,C,G,QSUM" > READSTATS.csv
cat *_READSTATS.csv >> READSTATS.csv
rm *_READSTATS.csv
sed -i 's/.fastq//g' READSTATS.csv
sed -i 's/_/,/g' READSTATS.csv
~~~~~~~~~~~

...then analyse *READSTATS.csv* in R via:

~~~~~~~~~~~~
#!/usr/bin/Rscript
library(ggplot2)
library(gridExtra)

dat = read.csv("READSTATS.csv", header=TRUE)
dat = subset(dat, PASS=="pass")
dat = subset(dat, BARCODE=="barcode01" | BARCODE=="barcode02" | BARCODE=="barcode03" | BARCODE=="barcode04" | BARCODE=="barcode05" | BARCODE=="barcode06")
dat = droplevels(dat)

dat$PERC_GC = (dat$G+dat$C)/(dat$A+dat$T+dat$C+dat$G)
dat$Q = dat$QSUM/dat$READLENGTH

READ_LEN=aggregate(READLENGTH~BARCODE, data=dat, FUN=mean)
NUM_READS=aggregate(READLENGTH~BARCODE, data=dat, FUN=length)

jpeg("READLENGTHS_STATS.jpeg", width=1000, height=700)
    p <- ggplot(dat, aes(x=BARCODE, y=READLENGTH)) +
    geom_violin(trim=FALSE, fill="gray") + theme_classic() +
    coord_cartesian(ylim=c(-5000, max(dat$READLENGTH))) +
    geom_boxplot(width=0.1) +
    annotate("text", x=c(1,2,3,4,5,6), y=-2000, label=c(paste0(round(READ_LEN[,2], 0), " bp"))) +
    annotate("text", x=c(1,2,3,4,5,6), y=-4000, label=c(paste0(round(NUM_READS[,2], 0), " reads")))
    p
dev.off()

jpeg("GC_CONTENT.jpeg", width=800, height=700)
    par(mfrow=c(2,3))
    for(i in levels(dat$BARCODE)){
        hist(subset(dat, BARCODE==i)$PERC_GC, nclass=10, main=i, xlab="GC Content")
    }
dev.off()

jpeg("SEQUENCE_QUALITY.jpeg", width=1000, height=700)
    par(mfrow=c(2,3))
    for(i in levels(dat$BARCODE)){
        hist(subset(dat, BARCODE==i)$Q, nclass=10, main=i, xlab="Sequence Quality")
    }
dev.off()

jpeg("QUALITYxREAD_LENGTH.jpeg", width=1000, height=700)
    # #removing outliers a bit:
    # dat = dat[dat$READLENGTH<=10000, ]
    hist_x = ggplot() + geom_histogram(aes(dat$Q), bins=10) + theme_classic() + theme(axis.text.x=element_blank(), axis.text.y = element_blank(), axis.ticks=element_blank(), text=element_text(size=20), axis.text=element_text(size=20)) + labs(x="Phred Quality", y="")
    hist_y = ggplot() + geom_histogram(aes(dat$READLENGTH), bins=10) + coord_flip() + theme_classic() + theme(axis.text.x=element_blank(), axis.text.y = element_blank(), axis.ticks=element_blank(), text=element_text(size=20), axis.text=element_text(size=20)) + labs(x="Read Length", y="")
    scatter = ggplot() + geom_point(aes(x=dat$Q, y=dat$READLENGTH)) + labs(x="", y="")
    empty = ggplot()+geom_point(aes(1,1), colour="white")+
             theme(axis.ticks=element_blank(),
                   panel.background=element_blank(),
                   axis.text.x=element_blank(), axis.text.y=element_blank(),
                   axis.title.x=element_blank(), axis.title.y=element_blank())
    grid.arrange(hist_y, scatter, empty, hist_x, ncol=2, nrow=2, widths=c(1, 4), heights=c(4, 1))
dev.off()
~~~~~~~~~~~~

# DE NOVO ASSEMBLY

## Remove unwanted DNA fragments with *WIMP*

~~~~~~~~~~~
???
~~~~~~~~~~~

## Assemble with *miniasm*

But first install *minimap2* and *miniasm*:

~~~~~~~~~~~
cd ~/SOFTWARES/
git clone https://github.com/lh3/minimap2
cd minimap2 && make
cd ..
git clone https://github.com/lh3/miniasm
cd miniasm  && make
~~~~~~~~~~~

~~~~~~~~~~~
DIR=/data/Lolium/Genomics/SEQUENCES/DNA/MinION/002_Test_Pool_20180801/ASSEMBLY
cd $DIR

~/SOFTWARES/minimap2/minimap2 -x ava-ont -t10 \
    ../FASTQ_OUT/Lolium_pool.fastq \
    ../FASTQ_OUT/Lolium_pool.fastq \
    > Lolium_pool.paf

~/SOFTWARES/miniasm/miniasm -f ../FASTQ_OUT/Lolium_pool.fastq Lolium_pool.paf > Lolium_pool.gfa
~~~~~~~~~~~

## Assess assembly quality (N50 + coverage against the *Lolium perenne* genome)

~~~~~~~~~~~
???
~~~~~~~~~~~

## Alternative assembly with *canu*

~~~~~~~~~~~
DIR=/mnt/Lolium_rigidum
~/SOFTWARES/canu*/Linux-amd64/bin/canu \
    -d ${DIR}/ASSEMBLY \
    -p Lrigidum_TEST01 \
    genomeSize=2g \
    -nanopore-raw  ${DIR}/FASTQ/*.fastq \
    correctedErrorRate=0.040 \
    corMhapSensitivity="low" \
    corMhapMemory=60 \
    gnuplotTested=true \
    merylMemory=60 \
    batMemory=60 \
    cnsMemory=60

 mv /mnt/Lolium_rigidum/ASSEMBLY/Lrigidum_TEST01.gkpStore.BUILDING \
     /mnt/Lolium_rigidum/ASSEMBLY/Lrigidum_TEST01.gkpStore.ACCEPTED

correctedErrorRate=0.040 corMhapSensitivity="low"
gnuplotTested=true merylMemory=60  batMemory=60 cnsMemory=60
~~~~~~~~~~~
