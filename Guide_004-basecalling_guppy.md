---
title: Basecalling with GUPPY
author: jeff
date: 2020 February 26
documentclass: scrartcl
papersize: a4
geometry: "left=1cm, right=1cm, top=2cm, bottom=2cm"
fontsize: 9pt
tags: [minion, MinKNOW, nanopore, nanoporetech, Oxford Nanopore]
titlepage: true
# titlepage-color: 4daf4a
titlepage-color: 1F78B4
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "FFFFFF"
titlepage-rule-height: 2
---

# Installation

~~~~~~~~~~
sudo apt-get update
sudo apt-get install wget
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt bionic-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt-get update
sudo apt-get install minion-nc

sudo apt-get update
sudo apt-get install wget lsb-release
PLATFORM=$(lsb_release -cs)
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt ${PLATFORM}-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt-get update
sudo apt-get install ont-guppy ### gpu version
# sudo apt-get install ont-guppy-cpu ### cpu-only

### OR
wget https://mirror.oxfordnanoportal.com/software/analysis/ont-guppy-cpu_3.4.5_linux64.tar.gz
tar -xvzf ont-guppy-cpu_3.4.5_linux64.tar.gz
~~~~~~~~~~

# Running

~~~~~~~~~~
guppy_basecaller \
   --input_path Full or relative path to the directory where the raw read files are located \
   --save_path Full or relative path to the directory where the basecalled results will be saved \
   --flowcell flowcell_version --kit sequencing_kit version \
   --config configuration file containing guppy parameters
~~~~~~~~~~

# Example

~~~~~~~~~~
/data/Lolium/Softwares/ont-guppy-cpu/bin/guppy_basecaller \
   --input_path /data/Misc/MinION_20200226_Tflavus/FAST5/Tflavus/20200225_0819_MN29323_FAM95039_b1a38b48/fast5/ \
   --save_path /data/Misc/MinION_20200226_Tflavus/FASTQ \
   --flowcell FLO-MIN106 \
   --kit SQK-LSK109 \
   --cpu_threads_per_caller 11
~~~~~~~~~~
