---
title: Rapid Sequencing for Testing the Variation in Coverage Between DNA Pools
author: Jeff Paril
date: 2018 September 05
documentclass: scrartcl
papersize: a4
geometry: "left=1cm, right=1cm, top=2cm, bottom=2cm"
fontsize: 9pt
tags: [minion, MinKNOW, nanopore, nanoporetech, Oxford Nanopore]
titlepage: true
# titlepage-color: 4daf4a
titlepage-color: 377eb8
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "FFFFFF"
titlepage-rule-height: 2
---

# Guide 001

## Rapid Sequencing for Testing the Variation in Coverage Between DNA Pools

# Software installation

## MinKNOW GUI and daemon
~~~~~~~~~~
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt update
sudo apt install minknow-nc
~~~~~~~~~~

## Albacore (basecalling software)
~~~~~~~~~~
wget https://mirror.oxfordnanoportal.com/software/analysis/python3-ont-albacore_2.3.1-1~xenial_amd64.deb
sudo dpkg -i python3-ont-albacore*.deb
~~~~~~~~~~

Several error messages will be printed out which will be fixed by installing missing dependencies via:

~~~~~~~~~~
sudo apt-get -f install
~~~~~~~~~~

Albacore installation craziness: remove albacore from the package list with pending problems via:

~~~~~~~~~~
sudo cp /var/lib/dpkg/status status.bkp
sudo nano /var/lib/dpkg/status
~~~~~~~~~~

Then remove from package: *albacore* until the nex package entry. And reinstall:

~~~~~~~~~~
sudo apt-get update
sudo apt-get install wget
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt trusty-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt-get update
sudo dpkg -i python3-ont-albacore*.deb
sudo apt-get -f install
~~~~~~~~~~

If you're still unable to properly install albacore, we'll use pip3 to install the wheel package:

~~~~~~~~~~
wget https://mirror.oxfordnanoportal.com/software/analysis/ont_albacore-2.3.1-cp36-cp36m-manylinux1_x86_64.whl
#Note: find the appropriate wheel package compatible with your python3 version from https://community.nanoporetech.com/downloads
pip3 install path/to/ont_albacore-xxx_x86_64.whl
~~~~~~~~~~

## Minimap2 (aligns and maps sequence reads supporting short and long reads)

~~~~~~~~~~~
git clone https://github.com/lh3/minimap2
cd minimap2 && make
~~~~~~~~~~~

# Running

## MinKNOW GUI
~~~~~~~~~~
cd /opt/ONT/MinKNOW
sudo bin/mk_manager_svc
~~~~~~~~~~

then in a separate terminal:

~~~~~~~~~~
MinKNOW
~~~~~~~~~~

or

~~~~~~~~~~
sudo systemctl daemon-reload
sudo systemctl enable minknow
sudo systemctl start minknow
MinKNOW
~~~~~~~~~~

# Updating
~~~~~~~~~~
sudo apt update
sudo apt install minknow-nc
~~~~~~~~~~

# Removal
~~~~~~~~~~
sudo apt purge minknow-nc
sudo apt autoremove
sudo apt clean
~~~~~~~~~~

# Hardware check

- plug in the device into a USB 3.0 port (blue)
- start MinKNOW
- check hardware by ticking the "Available" box and click on "Check hardware" button

# Flow cell check

- insert flow cell into the clip ensuring good contact between the flow cell and the MinION and close the lid
- select flow cell type (i.e. FLO-MIN106 or FLO-MIN107)
-  tick the available box and click on "Check flow cells" button

# Library preparation

1. Complexity reduction (double RE digest and size selection)

    + Materials:
        * genomic DNA samples
        * nuclease-free water (or sterile reverse osmosis (RO) water)
        * 200$\mu$l PCR tubes
        * 10X NEBuffer (compatible restriction enzyme buffer)
        * Enzyme 1 (10,000-20,000 U/mL)
        * Enzyme 2 (10,000-20,000 U/mL)
        * Thermocycler
        * P100 pipette and tips
        * P10 pipette and tips
        * NucleMag beads
        * 500$\mu$l eppy tubes
        * magnetic separator eppy tube rack
        * 80% ethanol

    + Dilute genomic DNA of each of the 5 pools to at least 200 ng/$\mu$l and place 5$\mu$l into PCR tube (or 0.5 ml tube) for a total of at least 1$\mu$g per tube
    + Prepare the master mix and add 45$\mu$l into each DNA-containing PCR tube
    + Digest at 37&deg;C for 1 hour, then deactivate enzymes at 75&deg;C for 20 minutes and hold at 16&deg;C
    + Run digested and undigested DNA in 1% agarose gel at 110V for ~15 minutes to check digestion
    + Volume up the digested DNA to 100$\mu$l
    + Remove large fragments by adding the appropriate volume of the homogenous bead solution (e.g. to remove >400bp add 40$\mu$l of the beads; 0.4:1 ratio)
    + Mix by pipetting up and down, and incubate at room temperature (RT) for 5 minutes
    + Magnet separate for $\ge$ 5 minutes and transfer 140$\mu$l supernatant to a new 500$\mu$l tube
    + Remove small fragments by adding the appropriate volume of the homogenous bead solution (e.g. to remove <150bp add 140$\mu$l of the beads; 1:1 ratio)
    + Mix by pipetting up and down, and incubate at RT for 5 minutes
    + Magnet separate for $\ge$ 5 minutes and discard supernatant
    + While on the magnetic rack, wash beads with 200$\mu$l 80% ethanol, mix by pipetting, incubate for 30 seconds, and remove supernatant carefully taking care to minimize pipetting out the beads
    + Repeat ethanol wash (after adding the ethanol, spin down to force the beads near the bottom of the tube to prepare for low volume elution)
    + Dry beads for 5-15 minutes at RT (**Do not over-dry the beads!**)
    + Elute the DNA in 47$\mu$l nuclease-free water, mix by flicking (the beads should resuspend easily if not over-dried), and incubate for 2-5 minutes
    + Magnet separate for 5 minutes and transfer DNA solution into a new 500$\mu$l eppy tube

| Reagents                | 1X 50$\mu$l Reaction ($\mu$l) |
|:------------------------|------------------------------:|
| Nuclease-free water     |                            38 |
| 10X NEBuffer (3.1)      |                             5 |
| Enzyme 1 (10k U/mL)     |                             1 |
| Enzyme 2 (10k U/mL)     |                             1 |
| gDNA (1$\mu$g)          |                             5 |

Table: Double restriction enzyme digestion mix

|Size (bp)|0.50:1|0.55:1|0.60:1|0.65:1|0.70:1|0.75:1|0.80:1|0.85:1|0.90:1|0.95:1|1:1|
|-----:|---:|---:|---:|---:|----:|----:|---:|---:|----:|----:|---:|
|  100 |  0 |  0 |  0 |  0 |   0 |   0 |  0 |  0 |   0 |   0 |  0 |
|  150 |  0 |  0 |  0 |  0 |   0 |   0 |  3 |  5 |   6 |   8 | 13 |
|  200 |  0 |  0 |  0 |  2 |   3 |   6 |  9 | 20 |  32 |  42 | 59 |
|  250 |  0 |  1 |  2 |  4 |   8 |  21 | 31 | 54 |  77 |  86 | 91 |
|  300 |  0 |  0 |  3 | 10 |  21 |  53 | 66 | 82 |  95 |  97 | 95 |
|  400 |  0 |  5 | 14 | 49 |  75 |  95 | 93 | 94 |  99 |  99 | 95 |
|  500 |  3 | 20 | 48 | 90 | 109 | 103 | 98 | 99 | 103 | 102 | 98 |
|  600 |  7 | 45 | 81 | 96 |  96 |  99 | 93 | 96 |  98 |  98 | 94 |
|  700 | 18 | 70 | 92 | 95 |  95 |  97 | 91 | 93 |  96 |  96 | 92 |
|  800 | 40 | 81 | 93 | 94 |  94 |  95 | 89 | 91 |  95 |  94 | 91 |
|  900 | 64 | 84 | 93 | 94 |  95 |  96 | 89 | 91 |  95 |  95 | 90 |
| 1000 | 80 | 83 | 91 | 93 |  94 |  95 | 88 | 90 |  94 |  94 | 89 |

Table: NucleoMag beads size selection guide. Values refer to percent DNA fragment recovery when using the ratios in the column header NucleoMag beads : DNA solution by volume.

2.  Quantification of digested and size-selected DNA samples (Qubit dsDNA HS quantification)

    + Materials:
        * size-selected DNA samples (1$\mu$l each)
        * nuclease-free water (or sterile reverse osmosis (RO) water)
        * 500$\mu$l Qubit tubes
        * P1000 pipette and tips
        * P100 pipette and tips
        * P10 pipette and tips
        * Qubit dsDNA HS Buffer
        * Qubit dsDNA HS Reagent (orange solution)
        * Qubit dsDNA HS Standard 1 (10$\mu$l)
        * Qubit dsDNA HS Standard 2 (10$\mu$l)

    + Prepare Qubit working solution: 1$\mu$l Qubit reagent + 199$\mu$l Qubit Buffer for each DNA sample (2 standards + number of DNA samples to quantify)
    + Place 1$\mu$l of DNA sample into each Qubit tubes and 10$\mu$l each of the 2 standards in separate tubes
    + Add the Qubit working solution, 199$\mu$l into the sample tubes and 190$\mu$l into the two standards
    + Load the standards first to calibrate
    + Read the samples one at a time (you may read the samples multiple times for replication)

3. End repair or A-tailing

    + Materials:
        * size-selected DNA samples
        * nuclease-free water (or sterile reverse osmosis (RO) water)
        * 500$\mu$l eppy tube
        * P100 pipette and tips
        * P10 pipette and tips
        * Ultra II End-Prep buffer
        * Ultra II End-Prep enzyme mix
        * Heat block
        * NucleMag beads
        * magnetic separator eppy tube rack
        * 70% ethanol

    + Dilute DNA samples to equal concentrations and volume up each sample to $\ge$ 45$\mu$l while targeting **$\ge$ 1000 ng of DNA material**
    + Prepare End-repair/dA-tailing master mix, add 15$\mu$l to each 45$\mu$l DNA solution, and mix by pipetting
    + Incubate at 20&deg;C (or RT) for 5 minutes and at 65&deg;C for 5 minutes
    + Add equal volume (60$\mu$l) of homogenized NucleoMag beads at RT, and mix by pipetting
    + Incubate at RT for 5 minutes, and pellet on magnet
    + While on the magnetic rack, wash with 200$\mu$l 70% ethanol and remove the supernatant without disturbing the beads
    + Repeat ethanol wash
    + Spin down, place on the magnet and pipette out residual wash with p10
    + Dry for ~5 minutes (**Do not over-dry the beads**)
    + Elute the DNA in 25$\mu$l nuclease-free water, mix by flicking, and incubate for 2-5 minutes
    + Magnet separate for 5 minutes and transfer dA-tailed DNA into a new 500$\mu$l eppy tube

| Reagents                     | 1X 60$\mu$l Reaction ($\mu$l) |
|:-----------------------------|------------------------------:|
| Nuclease-Free Water          |                             5 |
| Ultra II End-Prep Buffer     |                             7 |
| Ultra II End-Prep Enzyme Mix |                             3 |
| digested gDNA                |                            45 |

Table: End-repair or dA-tailing mix

4. Barcoding

    + Materials:
        * dA-tailed DNA samples
        * nuclease-free water (or sterile reverse osmosis (RO) water)
        * 500$\mu$l eppy tube
        * P100 pipette and tips
        * P10 pipette and tips
        * Native barcodes (NB01-NB12 from EXP-NBD103 barcoding kit)
        * Blunt/TA ligase master mix
        * NucleMag beads
        * magnetic separator eppy tube rack
        * 70% ethanol

    + Dilute dA-tailed DNA to ~500ng in 23.5$\mu$l
    + Prepare barcoding ligation reactions and mix each reaction by pipetting
    + Incubate at RT for 10 minutes
    + Add equal volume (30$\mu$l) of homogenized NucleoMag beads and mix by pipetting
    + Incubate at RT for 5 minutes, magnet separate, and discard supernatant
    + While on the magnetic rack, wash with 200$\mu$l 70% ethanol and remove the supernatant without disturbing the beads
    + Repeat ethanol wash
    + Spin down to force the beads near the bottom of the well to prepare for low elution volume for concentrated DNA solution; and pipette out residual wash with p10
    + Dry for ~5 minutes  (**Do not over-dry the beads**)
    + Elute the DNA in 16$\mu$l nuclease-free water, mix by flicking, and incubate for 2-5 minutes
    + Magnet separate for 5 minutes and transfer barcoded DNA into a new 500$\mu$l eppy tube


| Reagent                    | 1X 30$\mu$l Reaction ($\mu$l) |
|:---------------------------|------------------------------:|
| dA-tailed DNA              |                          23.5 |
| Barcode (each)             |                           1.5 |
| Blunt/TA Ligase Master Mix |                           5.0 |

Table: Barcoding ligation reaction mix

5. Quantification of digested, size-selected, dA-tailed, barcoded DNA samples (Qubit dsDNA HS quantification)

    + Materials:
        * size-selected DNA samples (1$\mu$l each)
        * nuclease-free water (or sterile reverse osmosis (RO) water)
        * 200$\mu$l Qubit tubes
        * P1000 pipette and tips
        * P100 pipette and tips
        * P10 pipette and tips
        * Qubit dsDNA HS Buffer
        * Qubit dsDNA HS Reagent (orange solution)
        * Qubit dsDNA HS Standard 1 (10$\mu$l)
        * Qubit dsDNA HS Standard 2 (10$\mu$l)

    + Prepare Qubit working solution: 1$\mu$l Qubit reagent + 199$\mu$l Qubit Buffer for each DNA sample (2 standards + number of DNA samples to quantify)
    + Place 1$\mu$l of DNA sample into each Qubit tubes and 10$\mu$l each of the 2 standards in separate tubes
    + Add the Qubit working solution, 199$\mu$l into the sample tubes and 190$\mu$l into the two standards
    + Load the standards first to calibrate
    + Read the samples one at a time (you may the samples multiple times for replication)

6. Ligation of MinION-specific adaptors (tethering with)

    + Materials:
        * barcoded DNA samples
        * nuclease-free water (or sterile reverse osmosis (RO) water)
        * 500$\mu$l eppy tube
        * P100 pipette and tips
        * P10 pipette and tips
        * Barcode Adapter Mix (BAM from EXP-NBD103 barcoding kit)
        * NEBNext Quick Ligation Reaction Buffer
        * Quick T4 DNA Ligase
        * NucleMag beads
        * magnetic separator eppy tube rack
        * Adapter Bead Binding Buffer (ABB from SQK-LSK108 ligation kit)
        * Elution Buffer (ELB from SQK-LSK108 ligation kit)
        * Heat block

    + Dilute each DNA sample into equal concentrations and pool to a total volume of 50$\mu$l containing a total of 700ng of DNA material (concentrate DNA via NucleoMag beads purification 1:1 ratio, if too dilute)
    + Prepare NEBNext Quick Ligation master mix, add equal volume (50$\mu$l) to the DNA samples, and mix by pipetting
    + Incubate at RT for 10 minutes
    + Add equal volume (100$\mu$l) of homogenized NucleoMag beads and mix by pipetting
    + Incubate at RT for 5 minutes, magnet separate, and discard supernatant
    + Add 140$\mu$l Adapter Bead Binding Buffer (ABB), mix by pipetting, magnet separate, and discard supernatant. Repeat this step.
    + Elute the DNA in 15$\mu$l elution buffer (ELB), mix by pipetting, and incubate at 37&deg;C for 10 minutes
    + Magnet separate for 5 minutes and transfer prepared DNA library into a new 500$\mu$l eppy tube (Optinal step to requentify with the aim of yielding ~200ng of DNA material)

| Reagents                               | 100$\mu$l Reaction ($\mu$l) |
|:---------------------------------------|----------------------------:|
| Barcoded DNA                           |                          50 |
| Barcode Adapter Mix (BAM)              |                          20 |
| NEBNExt Quick Ligation Reaction Buffer |                          20 |
| Quick T4 DNA Ligase                    |                          10 |

Table: NEBNext Quick Ligation mix


7. Prepare library for loading

 + Materials:
        * ligated barcoded dA-tailed size-selected digested genomic DNA samples
        * 500$\mu$l eppy tube
        * P100 pipette and tips
        * P10 pipette and tips
        * nuclease-free water (or sterile reverse osmosis (RO) water)
        * Running buffer with fuel mix (RBF from SQK-LSK108 ligation kit)
        * Library loading beads (LLB from SQK-LSK108 ligation kit)

    + Prepare the priming mix by mixing 480$\mu$l RBF with 520$\mu$l nuclease-free water
    + Mix 35$\mu$l RBF and 25.5$\mu$l LLB with 12$\mu$l of the adapted and tethered DNA library and volume up to 75$\mu$l with nuclease-free water

| Reagents                           | 100$\mu$l Reaction ($\mu$l) |
|:-----------------------------------|----------------------------:|
| Running Buffer with Fuel Mix (RBF) |                         480 |
| Nuclese-free Water                 |                         520 |

Table: Priming mix

| Reagents                           | 100$\mu$l Reaction ($\mu$l) |
|:-----------------------------------|----------------------------:|
| DNA library (adapted and tethered) |                        12.0 |
| Running Buffer with Fuel Mix (RBF) |                        35.0 |
| Library Loading Beads (LLB)        |                        25.5 |
| Nuclease-free Water                |                         2.5 |

Table: Library loading mix


# Flow cell priming and sample loading

- Warnings:
    + **it is essential that the sensor array remains submerged in buffer at all times**
    + **if an air bubble passes over any channels, those pores will be permanently damaged**
    + **avoid introducing air in the pipette tips**
    + **do not fully expel the liquid from the tip - leave a small volume in the tip**
    + **remove any air bubbles in the flow cell ports**
    + **priming and loading sample into flow cells is best done with minimal movement**
    + **do the following steps on the MinION device on a flat surface**
- Open the priming port by sliding the cover clockwise
- Remove inlet channel air plug
    + set P1000 pipette to 200$\mu$l
    + insert the tip into the priming port
    + keeping the tip inserted in the port, adjust the volume by twisting counter-clockwise until you reach 220-230$\mu$l, or until you see a small amount of the yellow buffer in the tip
    + remove pipette from the port and discard the tip
- Flow cell priming
    + prepare the priming mix (480$\mu$l RBF + 520$\mu$l nuclease-free water)
    + load 800$\mu$l of the priming mix into the priming port slowly
    + do not load all the mix keep a small amount at the tip
    + close the priming port
    + wait for 5 minutes
    + re-open the priming port by sliding the cover clockwise
    + open the sample port by gently lifting the sample port cover
    + load the remaining 200$\mu$l of the priming mix into the priming port using P1000 slowly to prevent overflowing in the sample port (do not empty the tip)
- Resuspend the library loading mix by gently pipetting up and down with P100
- Load 75$\mu$l of the the library loading mix into the sample port using P100 **one drop at a time** (wait for the previous drop to drain into the flow cell before adding the next drop)
- Close the sample port with the cover (ensure the bung enters the port)
- Close the priming port (turn counter-clockwise)
- Close the MinION lid carefully (magnetic at the edge)

# Sequencing run

- Open MinKNOW:

~~~~~~~~~~
sudo systemctl daemon-reload
sudo systemctl enable minknow
sudo systemctl start minknow
cd /opt/ui/
./MinKNOW
~~~~~~~~~~

- Tick the "Available" box and choose the proper flow cell type (i.e. FLO-MIN106 or FLO-MIN107)
- Click "New Experiment" and in the pop-up screen:
    + enter experiment name
    + select the proper kit (e.g. SQK-LSK108)
    + turn basecalling on or off depending on your machine (at least 16GB RAM and 1TB SSD is needed if you want this turned on)
    + set run options or keep default at 48 hours and -180mV
    + specify output files (just .fast5 files for separate basecalling; the number of files written per folder can also be specified)
    + Click "Start run"
- When sequencing is initiated:
    + the software will move to the channel screen
    + flow cell will cycle through all 2048 channels to select the best set of 512 to start the run
    + once the best combination of channels is selected, sequencing will begin on those channels automatically
    + within the first half an hour you should have most active pores sequencing and already have obtained several thousand reads
- After 6 hours (or however long you set):
    + you can stop the run from the home screen
    + output will be in /var/lib/MinKNOW/data/reads

# Washing and storing the flow cell for future use

- Open the priming port by turning the cover 90&deg; clockwise
- Remove all the buffer from the waste port
- Remove bubbles in the small section between the priming port and the sensor array by inserting p1000 tip into the priming port and twisting the pipette to increase the volume until the bubbles are captured/removed
- Add 150$\mu$l of Washing Kit Solution A through the priming port slowly, and wait 10 minutes
- For later re-use or for storage:
    + Slowly add 500$\mu$l of Washing Kit Storage Buffer (Solution S) through the priming port
    + Close the priming port and store flow cell at 4-8&deg;C
- For immediate re-use:
    +  Slowly add 150$\mu$l of Washing Kit Solution B
    +  Re-prime the flow cell

# Basecalling

Locate output files in /var/lib/MinKNOW/data/reads/{experiment_name}/. Copy *.fast5* files into a high performance computing machine and move all *.fast5* files into a single directory:

~~~~~~~~~~~
scp /var/lib/MinKNOW/data/reads/20180_*/ user@123.456.789://some/directory/RAW_DATA

ssh user@123.456.789
cd /some/directory/RAW_DATA
for d1 in 201808*/
do
for d2 in ${d1}fast5/*/
do
echo $d2
mv ${d2}*.fast5 .
done
done
~~~~~~~~~~~

Convert fast5 raw reads into fastq sequence format via basecalling with *albacore*:

~~~~~~~~~~~
DIR=/data/Lolium/Genomics/SEQUENCES/DNA/MinION/002_Test_Pool_20180801

read_fast5_basecaller.py \
    --flowcell FLO-MIN106 \
    --kit SQK-LSK108 \
    --barcoding \
    --output_format fastq \
    --input ${DIR}/RAW_DATA \
    --save_path ${DIR}/FASTQ_OUT \
    --worker_threads 10
~~~~~~~~~~~

Merge *.fastq files per barcode:

~~~~~~~~~~~~
DIR=/data/Lolium/Genomics/SEQUENCES/DNA/MinION/002_Test_Pool_20180801

cd ${DIR}/FASTQ_OUT/workspace/
for i in $(ls)
do
    cd $i
    for j in $(ls)
    do
        cat ${j}/*.fastq > ../../${i}_${j}.fastq
    done
    cd ..
done

cd ..
rm -R workspace
~~~~~~~~~~~~

Assess read lengths and number of reads:

~~~~~~~~~~~~
DIR=/data/Lolium/Genomics/SEQUENCES/DNA/MinION/002_Test_Pool_20180801

cd ${DIR}/FASTQ_OUT/
echo "PASS,BARCODE,READLENGTH" > READLENGTHS.csv
for i in *.fastq
do
    echo $i
    LEN=$(grep -c "@" $i)
    for j in $(seq 1 $LEN)
    do
        LINE=$(echo "($j * 4) - 2" | bc)
        #echo ${i%.*},$(head -n$(echo "($j * 4) - 2" | bc) $i | tail -n1 | wc -c) >> READLENGTHS.csv #PAINFULLY SLOWWWW!!!
        echo ${i%.*},$(sed "${LINE}q;d" $i | wc -c) >> READLENGTHS.csv
    done
done
sed -i 's/_/,/g' READLENGTHS.csv
~~~~~~~~~~~~

...then analyse *READLENGTHS.csv* in R via

~~~~~~~~~~~~
#!/usr/bin/Rscript
dat=read.csv("READLENGTHS.csv", header=TRUE)
dat = subset(dat, PASS=="pass")

READ_LEN=aggregate(READLENGTH~BARCODE, data=dat, FUN=mean)
NUM_READS=aggregate(READLENGTH~BARCODE, data=dat, FUN=length)

jpeg("READLENGTHS_STATS.jpeg", width=1000, height=700)
    BP = boxplot(READLENGTH~BARCODE, data=dat)
    boxplot(READLENGTH~BARCODE, data=dat)
    text(x=1:nlevels(dat$BARCODE), y=BP$stats[nrow(BP$stats),] + 3400, labels=paste(round(READ_LEN[,2], 0), " bp"))
    text(x=1:nlevels(dat$BARCODE), y=BP$stats[nrow(BP$stats),] + 3000, labels=paste0(round(NUM_READS[,2], 0), " reads"))
dev.off()
~~~~~~~~~~~~

Alternatively, you can use *fastQC* to asses sequence quality which is usually suboptimal because of the currently expected high error rate in nanopore sequencing:

~~~~~~~~~~~
cd /data/Lolium/Softwares
wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.7.zip
unzip fastqc*
cd FastQC
chmod +x fastqc

DIR=/data/Lolium/Genomics/SEQUENCES/DNA/MinION/003*/
cd $DIR
mkdir FASTQ/fastqc_out
/data/Lolium/Softwares/FastQC/fastqc ${DIR}/FASTQ/*.fastq
~~~~~~~~~~~
