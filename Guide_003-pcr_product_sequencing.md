---
title: PCR Product Sequencing (1/4 Reaction Volumes)
author: PhyB gene
date: 2019 October 02
documentclass: scrartcl
papersize: a4
geometry: "left=1cm, right=1cm, top=2cm, bottom=2cm"
fontsize: 9pt
tags: [minion, MinKNOW, nanopore, nanoporetech, Oxford Nanopore]
titlepage: true
# titlepage-color: 4daf4a
titlepage-color: 1F78B4
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "FFFFFF"
titlepage-rule-height: 2
---

# PCR Product Sequencing

# Run MinKNOW

~~~~~~~~~~
sudo systemctl daemon-reload
sudo systemctl enable minknow
sudo systemctl start minknow
cd /opt/ONT/MinKNOW
MinKNOW
~~~~~~~~~~

# Hardware check

- plug in the device into a USB 3.0 port (blue)
- start MinKNOW
- check hardware by ticking the "Available" box and click on "Check hardware" button

# Flow cell check

- insert flow cell into the clip ensuring good contact between the flow cell and the MinION and close the lid
- select flow cell type (i.e. FLO-MIN106 or FLO-MIN107)
-  tick the available box and click on "Check flow cells" button

# Library preparation

1. PCR product clean-up

| Amplicon clean-up | 1X (volumes) |          1X ($\mu$l) | ____X Reactions ($\mu$l) |
|:------------------|-------------:|---------------------:|-------------------------:|
| Amplicon          |            1 | ____________________ |                        - |
| NucleoMag beads   |            1 | ____________________ |     ____________________ |

Table: PCR product clean-up using magnetic beads

| Washing & Elution | 1 Sample ($\mu$l) | ___ Samples ($\mu$l) |
|:------------------|------------------:|---------------------:|
| Ethanol (80%)     |         2 x (200) | ____________________ |
| NFW               |                12 | ____________________ |

Table: Bead clean-up/purification washing and elution reagents

- Mix equal volumes of the PCR product and homogenised room-temperature (RT) NucleoMag beads
- Mix by pipetting up and down, and incubate at RT for 5 minutes
- Magnet separate for $\ge$ 5 minutes and discard supernatant
- While on the magnetic rack, wash beads with 200$\mu$l 80% ethanol, mix by pipetting, incubate for 30 seconds, and remove supernatant carefully taking care to minimize pipetting out the beads
- Repeat ethanol wash (after adding the ethanol, spin down to force the beads near the bottom of the tube to prepare for low volume elution)
- Dry beads for 5-15 minutes at RT (**Do not over-dry the beads!**)
- Elute the DNA in 15$\mu$l nuclease-free water (11.25$\mu$l end repair + 1$\mu$l Qubit + 2.75$\mu$l extra), mix by flicking (the beads should resuspend easily if not over-dried), and incubate for 2-5 minutes
- Magnet separate for 5 minutes and transfer DNA solution into a new 500$\mu$l eppy tube

2.  Amplicon quantification

| Qubit Working Solution | 1 Sample  ($\mu$l) | ____X Reactions ($\mu$l) |
|:-----------------------|-------------------:|-------------------------:|
| Qubit Buffer           |                199 |     ____________________ |
| Qubit Reagent          |                  1 |     ____________________ |

Table: Qubit working solution

| Samples    | 1 Sample  ($\mu$l) |
|:-----------|-------------------:|
| Amplicon   |                  1 |
| Standard 1 |                 10 |
| Standard 2 |                 10 |

Table: DNA sample and standards

| Samples    | Qubit Working Solution  ($\mu$l) |
|:-----------|---------------------------------:|
| Amplicon   |                              199 |
| Standard 1 |                              190 |
| Standard 2 |                              190 |

Table: Working solution + samples and standards

- Prepare Qubit working solution: 1$\mu$l Qubit reagent + 199$\mu$l Qubit Buffer for each DNA sample (2 standards + number of DNA samples to quantify)
- Place 1$\mu$l of DNA sample into each Qubit tubes and 10$\mu$l each of the 2 standards in separate tubes
- Add the Qubit working solution, 199$\mu$l into the sample tubes and 190$\mu$l into the two standards
- Load the standards first to calibrate
- Read the samples one at a time (you may read the samples multiple times for replication)

3. End repair or A-tailing

| Reagents Dilution            | 4X Reagents ($\mu$l) | 4X NFW ($\mu$l) | ____X Reagents ($\mu$l) |  ____X NFW ($\mu$l) |
|:-----------------------------|---------------------:|----------------:|------------------------:|--------------------:|
| Ultra II End-Prep Buffer     |                    7 |               1 |     ___________________ | ___________________ |
| Ultra II End-Prep Enzyme Mix |                    3 |               4 |     ___________________ | ___________________ |

Table: End repair reagents dilution

| End repair Reaction                  | 1X ($\mu$l) |      ____X ($\mu$l) |
|:-------------------------------------|------------:|--------------------:|
| Diluted Ultra II End-Prep Buffer     |        2.00 | ___________________ |
| Diluted Ultra II End-Prep Enzyme Mix |        1.75 | ___________________ |
| Amplicon (~1000ng)                   |       11.25 |                   - |

Table: End repair reactions

- Dilute DNA samples to equal concentrations and volume up each sample to $\ge$ 11.25$\mu$l while targeting **$\ge$ 1000 ng of DNA material**
- Prepare End-repair/dA-tailing master mix, add 15$\mu$l to each 45$\mu$l DNA solution, and mix by pipetting
- Incubate at 20&deg;C (or RT) for 5 minutes and at 65&deg;C for 5 minutes
- Add equal volume (15$\mu$l) of homogenized NucleoMag beads at RT, and mix by pipetting
- Incubate at RT for 5 minutes, and pellet on magnet
- While on the magnetic rack, wash with 200$\mu$l 70% ethanol and remove the supernatant without disturbing the beads
- Repeat ethanol wash
- Spin down, place on the magnet and pipette out residual wash with p10
- Dry for ~5 minutes (**Do not over-dry the beads**)
- Elute the DNA in 5$\mu$l nuclease-free water (3$\mu$l barcoding + 1$\mu$l Qubit + 1$\mu$l extra), mix by flicking, and incubate for 2-5 minutes
- Magnet separate for 5 minutes and transfer dA-tailed DNA into a new 500$\mu$l eppy tube

4. Barcoding

| Reagents Dilution          | 4X Reagents ($\mu$l) | 4X NFW ($\mu$l) | ____X Reagents ($\mu$l) |  ____X NFW ($\mu$l) |
|:---------------------------|---------------------:|----------------:|------------------------:|--------------------:|
| Barcode (each)             |                  1.5 |             8.0 |     ___________________ | ___________________ |
| Blunt/TA Ligase Master Mix |                  5.0 |             3.6 |     ___________________ | ___________________ |

Table: Barcoding reagents dilution

| Barcoding Reaction                 | 1X Reaction ($\mu$l) | ____X Reaction ($\mu$l) |
|:-----------------------------------|---------------------:|------------------------:|
| Amplicon                           |        3.00 (~500ng) |                       - |
| Diluted Barcode (each)             |                 2.38 |     ___________________ |
| Diluted Blunt/TA Ligase Master Mix |                 2.15 |     ___________________ |

Table: Barcoding reactions

- Dilute dA-tailed DNA to ~500ng in 3$\mu$l
- Prepare barcoding ligation reactions and mix each reaction by pipetting
- Incubate at RT for 10 minutes
- Add equal volume (7.53$\mu$l) of homogenized NucleoMag beads and mix by pipetting
- Incubate at RT for 5 minutes, magnet separate, and discard supernatant
- While on the magnetic rack, wash with 200$\mu$l 70% ethanol and remove the supernatant without disturbing the beads
- Repeat ethanol wash
- Spin down to force the beads near the bottom of the well to prepare for low elution volume for concentrated DNA solution; and pipette out residual wash with p10
- Dry for ~5 minutes  (**Do not over-dry the beads**)
- Elute the DNA in 7$\mu$l nuclease-free water (1.05$\mu$l ligation + 1$\mu$l Qubit + 4.95$\mu$l extra), mix by flicking, and incubate for 2-5 minutes
- Magnet separate for 5 minutes and transfer barcoded DNA into a new 500$\mu$l eppy tube

5. Quantification of barcoded amplicons (Qubit dsDNA HS quantification)

- Optional step.

6. Ligation of MinION-specific adaptors (tethering with)

| Reagents                                     | 25$\mu$l Reaction ($\mu$l) |
|:---------------------------------------------|---------------------------:|
| Barcoded Amplicons (12 x 1.05$\mu$l; ~700ng) |                       12.5 |
| Barcode Adapter Mix (BAM)                    |                        5.0 |
| NEBNExt Quick Ligation Reaction Buffer       |                        5.0 |
| Quick T4 DNA Ligase                          |                        5.0 |

Table: Ligation reaction

- Dilute each DNA sample into equal concentrations and pool for a total volume of 12.5$\mu$l containing a total of ~700ng of DNA material (concentrate DNA via NucleoMag beads purification 1:1 ratio, if too dilute)
- Add equal volume of the ligation master mix (25$\mu$l) to the DNA samples, and mix by pipetting
- Incubate at RT for 10 minutes
- Add equal volume (25$\mu$l) of homogenized NucleoMag beads and mix by pipetting
- Incubate at RT for 5 minutes, magnet separate, and discard supernatant
- Add 35$\mu$l Adapter Bead Binding Buffer (ABB), mix by pipetting, magnet separate, and discard supernatant. Repeat this step.
- Elute the DNA in 15$\mu$l elution buffer (ELB; 12$\mu$l loading + 3$\mu$l extra), mix by pipetting, and incubate at 37&deg;C for 10 minutes
- Magnet separate for 5 minutes and transfer prepared DNA library into a new 500$\mu$l eppy tube (Optinal step to requentify with the aim of yielding ~200ng of DNA material)

7. Prepare library for loading

 + Materials:
        * ligated barcoded dA-tailed size-selected digested genomic DNA samples
        * 500$\mu$l eppy tube
        * P100 pipette and tips
        * P10 pipette and tips
        * nuclease-free water (or sterile reverse osmosis (RO) water)
        * Running buffer with fuel mix (RBF from SQK-LSK108 ligation kit)
        * Library loading beads (LLB from SQK-LSK108 ligation kit)

    + Prepare the priming mix by mixing 480$\mu$l RBF with 520$\mu$l nuclease-free water
    + Mix 35$\mu$l RBF and 25.5$\mu$l LLB with 12$\mu$l of the adapted and tethered DNA library and volume up to 75$\mu$l with nuclease-free water

| Reagents                           | 100$\mu$l Reaction ($\mu$l) |
|:-----------------------------------|----------------------------:|
| Running Buffer with Fuel Mix (RBF) |                         480 |
| Nuclese-free Water                 |                         520 |

Table: Priming mix

| Reagents                           | 100$\mu$l Reaction ($\mu$l) |
|:-----------------------------------|----------------------------:|
| DNA library (adapted and tethered) |                        12.0 |
| Running Buffer with Fuel Mix (RBF) |                        35.0 |
| Library Loading Beads (LLB)        |                        25.5 |
| Nuclease-free Water                |                         2.5 |

Table: Library loading mix


# Flow cell priming and sample loading

- Warnings:
    + **it is essential that the sensor array remains submerged in buffer at all times**
    + **if an air bubble passes over any channels, those pores will be permanently damaged**
    + **avoid introducing air in the pipette tips**
    + **do not fully expel the liquid from the tip - leave a small volume in the tip**
    + **remove any air bubbles in the flow cell ports**
    + **priming and loading sample into flow cells is best done with minimal movement**
    + **do the following steps on the MinION device on a flat surface**
- Open the priming port by sliding the cover clockwise
- Remove inlet channel air plug
    + set P1000 pipette to 200$\mu$l
    + insert the tip into the priming port
    + keeping the tip inserted in the port, adjust the volume by twisting counter-clockwise until you reach 220-230$\mu$l, or until you see a small amount of the yellow buffer in the tip
    + remove pipette from the port and discard the tip
- Flow cell priming
    + prepare the priming mix (480$\mu$l RBF + 520$\mu$l nuclease-free water)
    + load 800$\mu$l of the priming mix into the priming port slowly
    + do not load all the mix keep a small amount at the tip
    + close the priming port
    + wait for 5 minutes
    + re-open the priming port by sliding the cover clockwise
    + open the sample port by gently lifting the sample port cover
    + load the remaining 200$\mu$l of the priming mix into the priming port using P1000 slowly to prevent overflowing in the sample port (do not empty the tip)
- Resuspend the library loading mix by gently pipetting up and down with P100
- Load 75$\mu$l of the the library loading mix into the sample port using P100 **one drop at a time** (wait for the previous drop to drain into the flow cell before adding the next drop)
- Close the sample port with the cover (ensure the bung enters the port)
- Close the priming port (turn counter-clockwise)
- Close the MinION lid carefully (magnetic at the edge)

# Sequencing run

- Open MinKNOW:

~~~~~~~~~~
sudo systemctl daemon-reload
sudo systemctl enable minknow
sudo systemctl start minknow
cd /opt/ui/
./MinKNOW
~~~~~~~~~~

- Tick the "Available" box and choose the proper flow cell type (i.e. FLO-MIN106 or FLO-MIN107)
- Click "New Experiment" and in the pop-up screen:
    + enter experiment name
    + select the proper kit (e.g. SQK-LSK108)
    + turn basecalling on or off depending on your machine (at least 16GB RAM and 1TB SSD is needed if you want this turned on)
    + set run options or keep default at 48 hours and -180mV
    + specify output files (just .fast5 files for separate basecalling; the number of files written per folder can also be specified)
    + Click "Start run"
- When sequencing is initiated:
    + the software will move to the channel screen
    + flow cell will cycle through all 2048 channels to select the best set of 512 to start the run
    + once the best combination of channels is selected, sequencing will begin on those channels automatically
    + within the first half an hour you should have most active pores sequencing and already have obtained several thousand reads
- After 6 hours (or however long you set):
    + you can stop the run from the home screen
    + output will be in /var/lib/MinKNOW/data/reads

# Washing and storing the flow cell for future use

- Open the priming port by turning the cover 90&deg; clockwise
- Remove all the buffer from the waste port
- Remove bubbles in the small section between the priming port and the sensor array by inserting p1000 tip into the priming port and twisting the pipette to increase the volume until the bubbles are captured/removed
- Add 150$\mu$l of Washing Kit Solution A through the priming port slowly, and wait 10 minutes
- For later re-use or for storage:
    + Slowly add 500$\mu$l of Washing Kit Storage Buffer (Solution S) through the priming port
    + Close the priming port and store flow cell at 4-8&deg;C
- For immediate re-use:
    +  Slowly add 150$\mu$l of Washing Kit Solution A
    +  Re-prime the flow cell

# Basecalling

**Albacore Basecalling**

## Installation:

### Install dependencies:

~~~~~~~~~~
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt update
sudo apt install minknow-nc
~~~~~~~~~~

### Notes: Find the appropriate wheel package compatible with your python3 version from https://community.nanoporetech.com/downloads. Or use
~~~~~~~~~~
sudo -H with pip3
~~~~~~~~~~

~~~~~~~~~~
wget https://mirror.oxfordnanoportal.com/software/analysis/ont_albacore-2.3.1-cp36-cp36m-manylinux1_x86_64.whl
pip3 install path/to/ont_albacore-xxx_x86_64.whl
~~~~~~~~~~

### Install fast5 parser from multi-reads format to single-reads format

## Running:

~~~~~~~~~~
multi_to_single_fast5 -i /path/to/fast5_multi/ -s /path/to/save/fast5_single/

for i in $(ls /path/to/save/fast5_single/ | grep -v txt)
do
cp /path/to/save/fast5_single/${i}/*.fast5 /path/to/save/fast5_single/
done

read_fast5_basecaller.py \
    --flowcell FLO-MIN106 \
    --kit SQK-RAD004 \
    --output_format fastq \
    --input /path/to/save/fast5_single/ \
    --save_path /path/to/save/fastq/ \
    --worker_threads 10
~~~~~~~~~~

Example:

~~~~~~~~~~
multi_to_single_fast5 -i ~/chlamy_bcamm/RAW/ -s ~/chlamy_bcamm/FAST5/
for i in $(ls ~/chlamy_bcamm/FAST5/ | grep -v txt)
do
echo $i
cp ~/chlamy_bcamm/FAST5/${i}/*.fast5 ~/chlamy_bcamm/FAST5/
done
read_fast5_basecaller.py \
    --flowcell FLO-MIN106 \
    --kit SQK-RAD004 \
    --output_format fastq \
    --input ~/chlamy_bcamm/FAST5 \
    --save_path ~/chlamy_bcamm/FASTQ \
    --worker_threads 7
~~~~~~~~~~

## Reads analysis:

~~~~~~~~~~
wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.7.zip
unzip fastqc*
cd FastQC
chmod +x fastqc
./fastqc
~~~~~~~~~~
