---
title: Modified SQK-LSK109 shearing, ligation, and size selection
author: jeff
date: 2021 January 15
documentclass: scrartcl
papersize: a4
geometry: "left=1cm, right=1cm, top=2cm, bottom=2cm"
fontsize: 9pt
tags: [minion, MinKNOW, nanopore, nanoporetech, Oxford Nanopore, ONT, SQK-LSK109, ligation]
titlepage: true
titlepage-color: "a6d854"
titlepage-text-color: "FFFFFF"
titlepage-rule-color: "FFFFFF"
titlepage-rule-height: 2
---

# List of reagents and their functions in the protocol

- **LFB (long fragment buffer)** [[SQK-LSK109](https://store.nanoporetech.com/sample-prep/ligation-sequencing-kit.html)] - for washing the long fragment DNA - probably some hydrophilic substance that has similar function to alcohol or PEG?
- **EB (Oxford Nanopore Technologies (ONT)-provided elution buffer)** [[SQK-LSK109](https://store.nanoporetech.com/sample-prep/ligation-sequencing-kit.html)] - probably Tris (pH8) buffer?
- **FLT (flush tether)** [[SQK-LSK109:Flow Cell Priming Expansion Pack](https://store.nanoporetech.com/sample-prep/ligation-sequencing-kit.html)] - flushing and priming thing-o?
- **FB (flush buffer)** [[SQK-LSK109:Flow Cell Priming Expansion Pack](https://store.nanoporetech.com/sample-prep/ligation-sequencing-kit.html)] - buffer mixed with the flush tether (FLT) for flushing the storage buffer and priming the flow cell for library loading, i.e. aid in the generation of a negative pressure to suck-in the library through the SpotON port?
- **SQB (sequencing buffer)** [[SQK-LSK109](https://store.nanoporetech.com/sample-prep/ligation-sequencing-kit.html)] - buffer during nanopore sequencing for pH control (ph=?)
- **LB (loading bead suspension)** [[SQK-LSK109](https://store.nanoporetech.com/sample-prep/ligation-sequencing-kit.html)] - to uniformly disperse the DNA library?

- **Ethanol** - hydrophilic DNA washing agent
- **Ultra II End-Prep Buffer** [[NEBNext® Ultra™ II End Repair/dA-Tailing Module](https://www.nebiolabs.com.au/products/e7546-nebnext-ultra-ii-end-repair-da-tailing-module#Protocols,%20Manuals%20&%20Usage)] - buffer during DNA end repair where fragmented DNA with overhangs are converted into blunt-end DNA.
- **Ultra II End-Prep Enzyme Mix** [[NEBNext® Ultra™ II End Repair/dA-Tailing Module](https://www.nebiolabs.com.au/products/e7546-nebnext-ultra-ii-end-repair-da-tailing-module#Protocols,%20Manuals%20&%20Usage)] - contains T4 DNA Polymerases, Klenow fragments, and T4 Polynucleotide Kinase for efficient end repair.
- **LNB (ligation buffer)** [[SQK-LSK109](https://store.nanoporetech.com/sample-prep/ligation-sequencing-kit.html)] - buffer for ligating ONT adapters.
- **AMX (adapter mix)** [[SQK-LSK109](https://store.nanoporetech.com/sample-prep/ligation-sequencing-kit.html)] - adapters for ONT nanopore sequencing.
- **Quick T4 DNA ligase** [[Quick Ligation™ Kit](https://www.nebiolabs.com.au/products/m2200-quick-ligation-kit#Product%20Information)] - ligates the ONT adapters into the blunt-end DNA fragments.
- **Wash solution A** [[EXP-WSH003 - no longer available, and replaced by EXP-WSH004](https://store.nanoporetech.com/flow-cell-wash-kit-r9.html)] - contains DNAse I to digest residual DNA stuck in the nanopores.
- **Wash solution B** [[EXP-WSH003 - no longer available, and replaced by EXP-WSH004](https://store.nanoporetech.com/flow-cell-wash-kit-r9.html)] - residual DNA digestion buffer.
- **Wash solution S** [[EXP-WSH003 - no longer available, and replaced by EXP-WSH004](https://store.nanoporetech.com/flow-cell-wash-kit-r9.html)] - storage buffer.
- **Magnetic bead suspension** - contains salt and polyethylene glycol (PEG) to precipitate the DNA from a long water-coated polymer into a water-stripped condensed "sticky ball". These "sticky balls" of DNA can aggragate together and stick to other surfaces it collides with like the surfaces of magnetic beads via hydrogen bonding (not ionic bonding). Larger volume of this suspension provides more beads to capture more DNA, where larger fragments tend to generate larger clumps and are easier and therefore the first ones to stick to the magnetic beads. It follows that adding low volumes of this suspension relative to the DNA solution will capture more of the large fragments and less of the small fragments, and that the smaller fragments will be captured progressively more as the relative volume of the bead suspension increases.
- **Water (H₂O)** - is a polar solvent. Various molecular-grade forms exist including nuclease-free water (NFW), filtered and deionized water (Milli-Q), nanopure water (nH₂O), or double distilled water (ddH₂O).

# Shearing high molecular weight genomic DNA

1. Aliquot a total of 10 µg of high molecular weight genomic DNA (HMW gDNA), and volume up to 250 µl with nuclease-free water (NFW) (tubes set 1).
2. Shear the DNA by passing the solution in and out of a 26G needle syringe for 5 times (or 20 passes through a p1000 pipette tip). 
3. Add 250 µl (1 volume) of magnetic beads (1mg/ml of buffer; pre-made fomulation of [Macherey-Nagel™ NucleoMag™ NGS Clean-up and Size Select](https://www.fishersci.co.uk/shop/products/nucleomag-ngs-clean-up-size-select/15889167) or [Ampure XP](https://www.beckman.com.au/reagents/genomic/cleanup-and-size-selection/pcr)), and mix by gently flicking.
4. Incubate at room temprature for 10 minutes.
5. Place on a magnetic rack until the solution becomes clear (i.e. the DNA-bead complexes have migrated towards the magnet).
6. Keeping the tube on the magnetic rack, remove the supernatant by pipetting it out (do not disturb the pellet).
7. Keeping the tube on the magnetic rack, wash twice with 500 µl of 80% ethanol (do not disturb the pellet).
8. Keeping the tube on the magnetic rack, remove residual ethanol (the tube may be spun briefly and returned to the magnetic rack to improve the removal of residual ethanol).
8. Keeping the tube on the magnetic rack, air dry the pellet for 2 minutes (5 minutes at most; do not completely dry or the pellet will crack and reduce the yield).
9. Remove the tube from the magnetic rack, resuspend in 55 µl of NFW, and mix by gently flicking the tube.
10. Briefly spin down so that all the suspension collects at the bottom of the tube, and incubate at 37°C for 5 minutes.
11. Place on the magnetic rack until the solution becomes clear and transfer the supernatant containing the sheared DNA into a fresh tube (tubes set 2).
12. Quantify 2 µl of the DNA using Qubit DNA HS kit (target ~60% recovery or a total of ~6 µg of DNA recovered).

# End-repair

13. Prepare the **End-repair reaction mix** (60 µl):
    + 50 µl DNA
    + 7 µl **Ultra II End-Prep Buffer**
    + 3 µl **Ultra II End-Prep Enzyme Mix**
14. Incubate at 20°C for 30 minutes, and at 65°C for another 30 minutes.
15. Add 120 µl of NFW (for a total volume of 180 µl).
16. Add 180 µl (1 volume) of magnetic bead suspension, and mix by gently flicking.
17. Incubate at room temprature for 10 minutes.
18. Place on a magnetic rack until the solution becomes clear.
19. Keeping the tube on the magnetic rack, remove the supernatant by pipetting it out (do not disturb the pellet).
20. Keeping the tube on the magnetic rack, wash twice with 500 µl of **80% ethanol** (do not disturb the pellet).
21. Keeping the tube on the magnetic rack, remove residual ethanol (the tube may be spun briefly and returned to the magnetic rack to improve the removal of residual ethanol).
22. Keeping the tube on the magnetic rack, air dry the pellet for 2 minutes (at most 5 minutes; do not completely dry).
23. Remove the tube from the magnet, resuspend in 70 µl of NFW, and mix by gently flicking the tube.
24. Briefly spin down so that all the suspension collects at the bottom of the tube, and incubate at 37°C for 5 minutes.
25. Place on the magnetic rack until the solution becomes clear, and transfer the supernatant containing the sheared DNA into a fresh tube (tubes set 3).
26. Quantify 2 µl of the DNA using Qubit DNA HS kit (target ~75% recovery or a total of ~4.5 µg of DNA recovered).

# Ligation and size selection

27. Prepare the **Ligation reaction mix** (100 µl):
    + 66 µl DNA
    + 25 µl **LNB** (ligation buffer; not NEBNExt Quick Ligation Reaction Buffer)
    +  5 µl **AMX** (adapter mix)
    +  4 µl **Quick T4 DNA ligase**
28. Incubate at 20°C for 60 minutes.
29. Add 100 µl of NFW (for a total volume of 200 µl).
30. Add 80 µl of magnetic bead suspension (i.e. 2/5 volume), mix by flicking, and briefly spin.
31. Incubate at room temperature for 10 minutes.
32. Place on a magnetic rack until the solution becomes clear, and remove the supernatant.
33. Wash the beads with 250 µl of LFB, mix by flicking, briefly spin, place on the magnetic rack to pellet the beads, and remove the supernatant.
34. Repeat step 33 (i.e. wash the DNA-bead complexes with LFB again).
35. Keeping the tube on the magnetic rack, remove residual LFB.
36. Remove from the magnetic rack, add 25 µl of ONT-EB buffer, and mix by flicking.
37. Spin down and incubate at 37°C for 5 minutes.
38.  Place on a magnetic rack until the solution becomes clear, and transfer the supernatant containing the eluted DNA into a fresh tube (tubes set 4).
39. Quantify 2 µl of the DNA using Qubit DNA HS kit (target 50%-60% recovery or a total of ~2.5 µg of DNA recovered).

# Flowcell priming and library loading (200 to 400 ng DNA library per run, i.e. 6 to 12 sequencing runs)

40. Prepare the **library loading mix** (75 µl):
    + 37.5 µl **SQB** (sequencing buffer)
    + 25.5 µl **LLB** (library loading beads; bring to room temperature and mix thoroughly)
    + 12 µl DNA (diluted with NFW or concentrated using a lyophiliser for a total load of 200 to 400 ng DNA)
41. Open the MinION Mk 1B lid, slide the flow cell under the cli, and slide the *priming port* cover clockwise to open the *priming port* (do not close the *priming port* until step 46 to allow the flow cell pipe pressure dynamics to suck-in and disperse the library into the vicinity of the nanopores).
42. Set a p1000 pipette to 200 μl, insert the tip into the *priming port*, and turn the wheel to draw-in a small volume of the storage buffer (yellow fluid).
43. Prepare the flow cell **priming mix**: add 30 μl of thawed and mixed **FLT** (flush tether) directly to the tube of thawed and mixed **FB** (flush buffer), and mix by vortexing.
44. Load 800 μl of the **priming mix** into the flow cell via the *priming port*, avoid introducing air bubbles, and let it stand for 5 minutes.
45. Gently open the *SpotOn sample port* by lifting off its cover.
46. Load the remaining 200 μl of the **priming mix** into the flow cell via the *priming port*, avoid introducing air bubbles, and immediately perform the next step, i.e. step 47 (The flowcell may fail to "suck-in" the library if 5 minutes or longer have elapsed, which means the flow cell needs to be primed again, i.e. close the *SpotOn port* and repeat starting from step 42).
47. Mix the **library loading mix** by gently pipetting up and down, and slowly load it into the *SpotON sample port* one drop-at-a-time (all 75 µl of it).
48. Close the *SpotON sample port* (make sure the bung of the cover enters the port), close the *priming port*, and close the MinION Mk 1B lid.

# Sequencing

49. Initialize the MinKNOW software:
```
sudo systemctl daemon-reload
sudo systemctl enable minknow
sudo systemctl start minknow
cd /opt/ont/minknow-ui/
./MinKNOW
```
50. Tick the "Available" box and choose the proper flow cell type (i.e. FLO-MIN106 or FLO-MIN107).
51. Click "New Experiment" and in the pop-up screen:
    + enter the experiment name,
    + select the proper kit (i.e. SQK-LSK109),
    + turn basecalling on or off depending on your machine (at least 16GB RAM and 1TB SSD is needed if you want this turned on),
    + set run options or keep default at 48 hours and -180mV,
    + specify output files (just .fast5 files for separate basecalling; the number of files written per folder can also be specified), and
    + click "Start run".
52. When sequencing is initiated:
    + the software will move to the flow cell channel screen,
    + flow cell will cycle through all 2048 channels to select the best set of 512 to start the run,
    + once the best combination of channels is selected, sequencing will begin on those channels automatically, and
    + within the first half an hour you should have most active pores sequencing and already have obtained several thousand reads.
53. After 24 hours (or however long you set):
    + you can stop the run from the home screen, and
    + output will be in `/var/lib/MinKNOW/data/reads`.

# Washing and storing the flow cell for future use

54. Place **wash solution A** (DNAse) on ice, and mix by gently inverting the tube (do not vortex).
55. Thaw 1 tube of **wash solution B** at room temperature, mix thoroughly by vortexing, spin down, and put on ice.
56. Prepare the **wash mix** (400 µl):
    +  20 µl wash solution A (DNAse)
    + 380 µl wash solution B
    + Mix gently by pipetting (do not vortex).
57. Keeping the *priming port* and *SpotON sample port* closed, remove all the fluid from the waste port/s using a p1000 pipette.
58. Open the *priming port* by turning the cover 90° clockwise
59. Remove bubbles in the small section between the *priming port* and the sensor array by inserting p1000 tip into the *priming port* and twisting the pipette to increase the volume until the bubbles are removed (do not remove too muich liquid to avoid exposing the sensor array to air).
60. Load 400 µl of the **wash mix** into the *priming port* (avoid introducing air bubbles), and close the *priming port*.
61. Incubate at room temperature for 30 minutes (proceed to step 62 for storage, or prepare for another sequencing run).
62. Open the *priming port*, add 500 µl of **solution S** (avoid introducing air bubbles), close the *priming port*, and store at 4°C.

# Altenative wash using [NEB DNAse I](https://www.nebiolabs.com.au/products/m0303-dnase-i-rnase-free#Product%20Information)
- Prepare the wash mix.
    +  40 µl 10X DNAse I reaction buffer (1X: 10mM Tris-HCl, 2.5mM MgCl2, 0.5mM CaCl2, pH7.6; or prepare a modified buffer from [van der Heiden (2019), p40](https://stud.epsilon.slu.se/15244/7/van_der_heiden_191216.pdf): 300 mM KCl;  2 mM  CaCl2; 10mM  MgCl2; 15mM HEPES; pH8.0)
    +   4 µl DNAse I
    + 356 µl NFW
- Load into the priming port.
- Incubate at room temperature for 1 hour.
- Prime the flowcell again (i.e. repeat from step 40) or add storage buffer (i.e. step 62) for storage.

# References

- Brunker, Kirstyn. 2020. Washing a MinION flowcell. [dx.doi.org/10.17504/protocols.io.bddzi276](dx.doi.org/10.17504/protocols.io.bddzi276)
- Oxford Nanopore. 2020. Genomic DNA by Ligation (SQK-LSK109). Version: GDE_9063_v109_revS_14Aug2019. Last updated: 28/01/2020. [https://store.nanoporetech.com/media/wysiwyg/pdfs/SQK-LSK109/Genomic_DNA_by_Ligation_SQK-LSK109_-minion.pdf](https://store.nanoporetech.com/media/wysiwyg/pdfs/SQK-LSK109/Genomic_DNA_by_Ligation_SQK-LSK109_-minion.pdf)
- Tyson, John. 2020. Modified LSK109 ligation prep with needle shear and bead clean up. [dx.doi.org/10.17504/protocols.io.7emhjc6](dx.doi.org/10.17504/protocols.io.7emhjc6)
