sudo apt-get install wget
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt-get update
sudo apt-get install ont-guppy

### test
mkdir GUPPY_TEST_20191013/
guppy_basecaller \
  -i ~/BTCH90005_GROUPB/RAW/ \
  -s ~/BTCH90005_GROUPB/GUPPY_TEST_20191013/ \
  --flowcell FLO-MIN107 \
  --kit SQK-RAD004\
