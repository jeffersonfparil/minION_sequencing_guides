### blastn_test.r

LIST = system("ls *.blastout", intern=TRUE)

FNAME=c()
SEQID=c()
PIDEN=c()
EVALU=c()
BITSC=c()
TITLE=c()
pb = txtProgressBar(min=0, max=length(LIST), initial=0, style=3)
counter = 0
for (f in LIST){
  # f=LIST[1]
  dat = read.delim(f, header=FALSE)
  colnames(dat) = c("qseqid", "staxids", "pident", "evalue", "qcovhsp", "bitscore", "stitle")
  for (seqID in levels(dat$qseqid)){
    # seqID=levels(dat$qseqid)[1]
    sub = subset(dat, qseqid==seqID)
    sub = sub[order(sub$bitscore, decreasing=TRUE), ]
    FNAME = c(FNAME, f)
    SEQID = c(SEQID, as.character(sub[1,1]))
    PIDEN = c(PIDEN, sub[1,3])
    EVALU = c(EVALU, sub[1,4])
    BITSC = c(BITSC, sub[1,6])
    TITLE = c(TITLE, as.character(sub[1,7]))
  }
  counter = counter + 1
  setTxtProgressBar(pb, counter)
}
close(pb)

OUT = data.frame(FNAME, SEQID, PIDEN, EVALU, BITSC, TITLE)
OUT = OUT[order(OUT$BITSC, decreasing=TRUE), ]
write.csv(OUT, file="blastn_summarised_max_bitscores.csv")
