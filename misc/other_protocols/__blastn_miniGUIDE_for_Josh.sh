#!/bin/bash

for f in $(ls *.fastq)
do
	echo $f
	### convert fastq to fasta
	sed -n '1~4s/^@/>/p;2~4p' $f > fasta.fa
	### blastn and output a tab-delimited file
	blastn  -db /data/BlastDB/NCBI_NT0060 \
		-query fasta.fa \
		-perc_identity 95 \
		-outfmt '6 qseqid staxids pident evalue qcovhsp bitscore stitle' \
		-out ${f%.*}.blastout
done
rm fasta.fa
