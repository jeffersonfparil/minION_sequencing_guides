**Albacore Basecalling**

# Installation:

## Install dependencies:

wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | sudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | sudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
sudo apt update
sudo apt install minknow-nc

## Notes: Find the appropriate wheel package compatible with your python3 version from https://community.nanoporetech.com/downloads. Or use sudo -H with pip3

wget https://mirror.oxfordnanoportal.com/software/analysis/ont_albacore-2.3.1-cp36-cp36m-manylinux1_x86_64.whl
pip3 install path/to/ont_albacore-xxx_x86_64.whl

## Install fast5 parser from multi-reads format to single-reads format

# Running:

multi_to_single_fast5 -i /path/to/fast5_multi/ -s /path/to/save/fast5_single/

for i in $(ls /path/to/save/fast5_single/ | grep -v txt)
do
cp /path/to/save/fast5_single/${i}/*.fast5 /path/to/save/fast5_single/
done

read_fast5_basecaller.py \
    --flowcell FLO-MIN106 \
    --kit SQK-RAD004 \
    --output_format fastq \
    --input /path/to/save/fast5_single/ \
    --save_path /path/to/save/fastq/ \
    --worker_threads 10

Example:
multi_to_single_fast5 -i ~/chlamy_bcamm/RAW/ -s ~/chlamy_bcamm/FAST5/
for i in $(ls ~/chlamy_bcamm/FAST5/ | grep -v txt)
do
echo $i
cp ~/chlamy_bcamm/FAST5/${i}/*.fast5 ~/chlamy_bcamm/FAST5/
done
read_fast5_basecaller.py \
    --flowcell FLO-MIN106 \
    --kit SQK-RAD004 \
    --output_format fastq \
    --input ~/chlamy_bcamm/FAST5 \
    --save_path ~/chlamy_bcamm/FASTQ \
    --worker_threads 7

# Reads analysis:

wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.7.zip
unzip fastqc*
cd FastQC
chmod +x fastqc
./fastqc