### Analysing the Supplementary Data from Schalamin et al., 2018
### Citation:  Schalamun, M, Nagar, R, Kainer, D, et al. Harnessing the MinION: An example of how to establish long‐read sequencing in a laboratory using challenging plant tissue from Eucalyptus pauciflora. Mol Ecol Resour. 2019; 19: 77– 89. https://doi.org/10.1111/1755-0998.12938 

### Load the data
dat = read.csv("Schalamun et al 2018 Harnessing the MinION Eucalyptus pauciflora data.csv", na.strings=c("N.A.", "< 0"))
### Remove the first uninformative row
dat = dat[2:nrow(dat), ]

### Prepare the information we are interested in
Yield_Gb = dat$GB.data.called
DNA_extraction_ng.ul = dat$Qubit.ng.ul
DNA_input_endrep_ug = dat$Input.End.Repair..ug.
DNA_input_ligate_ug = dat$Input.Ligation..ug.
DNA_input_final_ng = dat$Final.library.input..ng.
Runtime = as.numeric(matrix(unlist(strsplit(as.character(dat$Flowcell.run.time), " ")), ncol=2, byrow=TRUE)[,1])
Size_selection = dat$Size.selection.
	Size_selection = as.character(Size_selection)
	Size_selection[Size_selection=="freeze thawing during grinding"] = "freeze thawed"
	Size_selection = as.factor(Size_selection)
Volumes_orig_v_long = dat$Library.prep.kit.type
FFPE_repair = dat$FFPE.repair

FACTORS_NUMERIC = list(DNA_extraction_ng.ul = DNA_extraction_ng.ul, 
					   DNA_input_endrep_ug = DNA_input_endrep_ug, 
					   DNA_input_ligate_ug = DNA_input_ligate_ug, 
					   DNA_input_final_ng = DNA_input_final_ng, 
					   Runtime = Runtime)
FACTORS_CATEGORICAL = list(Size_selection = Size_selection, 
						   Volumes_orig_v_long = Volumes_orig_v_long, 
						   FFPE_repair = FFPE_repair)

### Plot
svg("Schalamun et al 2018 Harnessing the MinION Eucalyptus pauciflora analysis.svg", width=12, height=7)
par(mfrow=c(2,4))
for (i in 1:length(FACTORS_NUMERIC)){
	# i =1
	f = unlist(FACTORS_NUMERIC[i])
	plot(x=f, y=Yield_Gb, xlab=names(FACTORS_NUMERIC[i]))
	mod = lm(Yield_Gb ~ f)
	a = coef(mod)[1]
	m = coef(mod)[2]
	newx = seq(from=min(f,na.rm=TRUE), to=max(f,na.rm=TRUE), length=100)
	newy = a + (m*newx)
	lines(x=newx, y=newy, lwd=2, lty=2)
	legend("topleft", legend=paste0("R2adj=", round(summary(mod)$adj.r.squared * 100), "%"))
}
for (i in 1:length(FACTORS_CATEGORICAL)){
	# i =1
	f = unlist(FACTORS_CATEGORICAL[i])
	# boxplot(Yield_Gb~f, xlab=names(FACTORS_CATEGORICAL[i]))
	### test 2021-01-29
	library(violinplotter)
	violinplotter(formula=Yield_Gb~f, TITLE=names(FACTORS_CATEGORICAL[i]))
}
dev.off()

### Naive linear model
model = lm(Yield_Gb ~ DNA_extraction_ng.ul +
					  DNA_input_endrep_ug +
					  DNA_input_ligate_ug +
					  DNA_input_final_ng +
					  Runtime +
					  Size_selection +
					  Volumes_orig_v_long +
					  FFPE_repair)
anova(model)
