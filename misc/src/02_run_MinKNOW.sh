#!/bin/bash

#activate MinKNOW daemon
gksudo systemctl daemon-reload
gksudo systemctl enable minknow
gksudo systemctl start minknow

#run MinKNOW GUI
cd /opt/ui/
./MinKNOW
