#!/bin/bash

#install MinKNOW: GUI for sequencing
wget -O- https://mirror.oxfordnanoportal.com/apt/ont-repo.pub | gksudo apt-key add -
echo "deb http://mirror.oxfordnanoportal.com/apt xenial-stable non-free" | gksudo tee /etc/apt/sources.list.d/nanoporetech.sources.list
gksudo apt update
gksudo apt install minknow-nc

#install Albacore: basecalling FAST5 to FASTQ conversion
PYTHON3_V=$(python3 --version)
case ${PYTHON3_V%.*} in
	"Python 3.4")
		wget https://mirror.oxfordnanoportal.com/software/analysis/ont_albacore-2.3.1-cp34-cp34m-manylinux1_x86_64.whl;;
	"Python 3.5")
		wget https://mirror.oxfordnanoportal.com/software/analysis/ont_albacore-2.3.1-cp35-cp35m-manylinux1_x86_64.whl;;
	"Python 3.6")
		wget https://mirror.oxfordnanoportal.com/software/analysis/ont_albacore-2.3.1-cp36-cp36m-manylinux1_x86_64.whl;;
esac
gksudo apt install pip3
pip3 install ont_albacore-*.whl

#install minimap2: aligning reads to some reference sequence
git clone https://github.com/lh3/minimap2
cd minimap2 && make

#install fast5 parser from multi-reads format to single-reads format
pip3 install ont-fast5-api