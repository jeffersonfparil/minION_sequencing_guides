#!/bin/bash

while test $# -gt 0
do
	case "$1" in
		-h|--help)
			echo -e "Converting Nanopore Sequencer's Electical Signals (fast5) into Sequence Data (fastq)"
			echo -e ""
			echo -e "Arguments:"
			echo -e "\t -h, --help \t\t help"
			echo -e "\t -f, --flowcell \t flow cell type: e.g. FLO-MIN106 or FLO-MIN107"
			echo -e "\t -k, --kit \t\t library preparation kit: e.g. SQK-LSK108 or SQK-RAD004"
			echo -e "\t -b, --barcoding \t barcodes used? 0 for FALSE and 1 for TRUE"
			echo -e "\t -i, --input-dir \t directory containing all the FAST5 files"
			echo -e "\t -o, --output-dir \t output directory"
			echo -e "\t -c, --threads \t\t number of threads to use"
			echo -e ""
		;;
		-f|--flowcell)
			export FLOWCELL=$1
		;;
		-k|--KIT)
			export KIT=$1
		;;
		-b|--barcodings)
			export BARCODING=$1
		;;
		-i|--input-dir)
			export INDIR=$1
		;;
		-o|--output-dir)
			export OUTDIR=$1
		;;
		-c|--threads)
			export THREADS=$1
		;;
	esac
	break
done

# # basecalling with albacore
# if [ -z ${BARCODING+x} ]
# then
# 	echo "Idicate whether or not you used barcodes in your library, e.g. --barcodes 0 for no or --barcodes 1 for yes."
# 	echo ""
	
# elif [ $BARCODING -eq 1 ]
# then
# 	read_fast5_basecaller.py \
# 	    --flowcell $FLOWCELL \
# 	    --kit $KIT \
# 	    --barcoding \
# 	    --output_format fastq \
# 	    --input $INDIR \
# 	    --save_path $OUTDIR \
# 	    --worker_threads $THREADS
# elif [ $BARCODING -eq 0 ]
# then
# 	read_fast5_basecaller.py \
# 	    --flowcell $FLOWCELL \
# 	    --kit $KIT \
# 	    --output_format fastq \
# 	    --input $INDIR \
# 	    --save_path $OUTDIR \
# 	    --worker_threads $THREADS
# else
# 	echo "Indicate whether or not you used barcodes in your library, e.g. --barcodes 0 for no or --barcodes 1 for yes."
# 	echo ""
# fi

### install guppy
wget https://mirror.oxfordnanoportal.com/software/analysis/ont-guppy-cpu_3.4.5_linux64.tar.gz
tar -xvzf ont-guppy-cpu_3.4.5_linux64.tar.gz

### basecalling with guppy
ont-guppy-cpu/bin/guppy_basecaller \
		--input_path ${INDIR} \
		--save_path ${OUTDIR} \
		--flowcell FLO-MIN106 \
		--kit ${KIT} \
		--cpu_threads_per_caller ${THREADS}
