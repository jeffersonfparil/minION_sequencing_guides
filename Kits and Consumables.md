# MinION Sequencing with Fragmentation, Ligation and Barcoding Costs per Reaction
[2019-02-12]

## Kits
1. Ligation Sequencing Kit 1D ([SQK-LSK109](https://store.nanoporetech.com/ligation-sequencing-kit.html); $599; 6 rxns -->  can be pushed to 12 reactions)
2. Native Barcoding Expansion 1-12 ([EXP-NBD103](https://store.nanoporetech.com/native-barcoding-expansion-1-29.html); $288; 6 rxns)
3. Flow Cell Wash Kit ([EXP-WSH002](https://store.nanoporetech.com/flow-cell-wash-kit-r9.html); $189; 12 rxns)

## Consumables
1. MseI ([R0525S](https://genesearch.com.au/prices/); $110;   500 units;  50$\mu$l)
2. NsiI ([R0127S](https://genesearch.com.au/prices/); $105; 1,000 units; 100$\mu$l)
3. NEBNext End repair / dA-tailing Module ([E7546S](https://genesearch.com.au/prices/); $473; 24 rxns)
4. NEB Blunt / TA Ligase Master Mix ([M0367S](https://genesearch.com.au/prices/); $185; 50 rxns)
5. NEBNext Quick Ligation Module ([E6056](https://genesearch.com.au/prices/); $579; 20 rxns)
6. [NucleoMag NGS Clean-up and Size Select](https://www.mn-net.com/ProductsBioanalysis/DNAandRNApurification/Cleanup/NucleoMagNGSCleanupandSizeSelect/tabid/12519/language/de-DE/Default.aspx)
7. Flowcell ([FLO-MIN106D; R9.4](https://store.nanoporetech.com/flowcells/spoton-flow-cell-mk-i-r9-4.html); $900; 3 uses)

## Breakdown of costs per sequencing run (one barcode)
| Material                               | Cost per reaction |
|:---------------------------------------|:-----------------:|
| Ligation Sequencing Kit 1D             |      $49.92       |
| Native Barcoding Expansion 1-12        | $4.00 per barcode |
| Flow Cell Wash Kit                     |      $15.75       |
| MseI                                   | $2.20 per $\mu$l  |
| NsiI                                   | $0.10 per $\mu$l  |
| NEBNext End repair / dA-tailing Module |      $19.71       |
| NEB Blunt / TA Ligase Master Mix       |       $3.70       |
| NEBNext Quick Ligation Module          |      $28.95       |
| **Total**                              |    **$124.33**    |

## Flowcell estimated cost
Assuming we can reuse the flowcell 3 times:

| Material     | Cost per reaction |
|:-------------|:-----------------:|
| **Flowcell** |    **$300.00**    |