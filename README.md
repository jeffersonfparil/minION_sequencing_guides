# minION_sequencing_guides

**A collection of guides in using Oxford Nanopore's MinION sequencing platform**

|**Website**|**License**|
|:-------:|:--------:|
| <a href="https://adaptive-evolution.biosciences.unimelb.edu.au/"><img src="misc/Adaptive Evolution Logo mod.png" width="75"> | [![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) |

## Convert markdown protocols into pdf:

- Install pandoc:
    + [https://pandoc.org/installing.html](https://pandoc.org/installing.html)
    + For example in ubuntu: `sudo apt install texlive-latex-base pandoc texlive-fonts-recommended`

- Convert:
```
pandoc -Ss Guide_001-rapid_sequencing.md \
	--latex-engine=xelatex \
	--template misc/template01.tex \
	--toc \
	--toc-depth=2 \
	--filter pandoc-eqnos \
	-o Guide_001-rapid_sequencing.pdf
```
